# year

获取当前日期获得年的部分

## Demo

```ts
import {year, date} from '@vill-v/date'

year(date())
```

## DTS

```ts
declare const year: (date: Date) => number;
```
