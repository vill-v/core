# date

当前时间

## Demo

```ts
import {date} from '@vill-v/date'

date()
```

## DTS

```ts
declare const date: () => Date;
```
