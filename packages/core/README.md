# vill-v
neta了崩坏3 往世乐土的英杰维尔薇

![npm](https://img.shields.io/npm/v/vill-v?style=flat-square)

**开发中对于技术的总结，归纳**

整合

[`@vill-v/vanilla`](https://www.npmjs.com/package/@vill-v/vanilla)

[`@vill-v/regexp-match`](https://www.npmjs.com/package/@vill-v/regexp-match)

[`@vill-v/broswer`](https://www.npmjs.com/package/@vill-v/broswer)

[`@vill-v/date`](https://www.npmjs.com/package/@vill-v/date)

[`@vill-v/ts`](https://www.npmjs.com/package/@vill-v/ts)

[`@vill-v/async`](https://www.npmjs.com/package/@vill-v/async)

[`@vill-v/type-as`](https://www.npmjs.com/package/@vill-v/type-as)

前身是 [@whitekite/utils](https://www.npmjs.com/package/@whitekite/utils)

_只是个人项目，整体较为随便，作为开发途中的经验总结，如有相似的需求，推荐copy或阅读源码，不建议将该包在实际项目中使用_

**如有幸被实际使用在项目内，不胜荣幸**
